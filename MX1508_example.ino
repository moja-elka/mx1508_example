/*
* MX1508_example.ino
*
* Author: https://moja-elka.blogspot.com
*/
#include "MX1508.h"

#define IN1 9
#define IN2 10
#define IN3 5
#define IN4 6

#define TURN_WITH_BOTH_MOTORS   0
#define MOVE_TIME               2000
#define STOP_TIME               4000
#define MOVE_SPEED              255

MX1508 leftMotor = MX1508(IN1, IN2);
MX1508 rightMotor = MX1508(IN3, IN4);

void setup()
{
    Serial.begin(9600);
    motorsStandby();
}

void loop()
{
    motorsForward(MOVE_SPEED);
    delay(MOVE_TIME);
    motorsBackward(MOVE_SPEED);
    delay(MOVE_TIME);
    motorsStop();
    delay(STOP_TIME);
    motorsTurnLeft(MOVE_SPEED);
    delay(MOVE_TIME);
    motorsTurnRight(MOVE_SPEED);
    delay(MOVE_TIME);
    motorsStop();
    delay(STOP_TIME);
}

void motorsForward(int speed)
{
    Serial.print(F("motorsForward: "));
    Serial.println(speed);
    forward(leftMotor, rightMotor, speed);
}

void motorsBackward(int speed)
{
    Serial.print(F("motorsBackward: "));
    Serial.println(speed);
    backward(leftMotor, rightMotor, speed);
}

void motorsTurnLeft(int speed)
{
    Serial.print(F("motorsTurnLeft: "));
    Serial.println(speed);
    left(leftMotor, rightMotor, speed, TURN_WITH_BOTH_MOTORS);
}

void motorsTurnRight(int speed)
{
    Serial.print(F("motorsTurnRight: "));
    Serial.println(speed);
    right(leftMotor, rightMotor, speed, TURN_WITH_BOTH_MOTORS);
}

void motorsStop()
{
    Serial.println(F("motorsStop"));
    brake(leftMotor, rightMotor);
}

void motorsStandby()
{
    standby(leftMotor, rightMotor);
}